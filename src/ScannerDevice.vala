/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Device {
	public string	uri { get; set; }
	public string	model { get; set; }
	public string	pilote { get; set; }
	public string[] modes { get; set; }
	public string[] sources { get; set; }
	public int low_quality { get; set; }
	public int high_quality { get; set; }
	public string[] resolutions { get; set; }

}

public class ScannerDevice: GLib.Object {
	public Device device;

	public bool has_scanner { get; set; default = false;}
	public bool is_scanning { get; set; default = false; }
	public bool has_gray { get; set; default = true; }

    private GLib.SList<string> scanned_files;
	public GLib.File output_dir;

    public string time_format_str;

	public signal void file_scanned (GLib.File file);
	public signal void scan_finished (GLib.SList<string> files);

	private string scanner_path_str;
	private GLib.KeyFile scanner_config;
	private GLib.KeyFile scanner_extend;

    private const string SCANNER_CONFIG_FILE = "pantheon-files/scanner.config"; // information from scanner -A
    private const string SCANNER_EXTEND_FILE = "pantheon-files/scanner-extends.config"; // information from scanner -A

    private const string ADF_DUPLEX_TRANSLATED = N_("ADF DUPLEX");
    private const string PLATEN_TRANSLATED = N_("PLATEN");
    private const string ADF_TRANSLATED = N_("ADF");

    public string LINEART_MODE = N_("Lineart");
    public string GRAYSCALE_MODE = N_("Gray");
    public string COLOR_MODE = N_("Color");

    private const string DEFAULT_SCAN_NAME = "Scan";

	private FileMonitor? monitor = null;

	private bool set_pdf;
    private GLib.File pdf_file;

	private GLib.File images_dir;

	private Gee.HashMap<string, string> source_map;

	public ScannerDevice (string output_directory) {
		scanned_files = null;


		this.device = new Device ();//device;

		GLib.DateTime time  = new GLib.DateTime.now ();
		time_format_str = time.format ("Scan %d %B %Y");

        scanner_path_str = GLib.Path.build_path (Path.DIR_SEPARATOR_S,
												 GLib.Environment.get_user_config_dir (),
												 SCANNER_CONFIG_FILE);

	    string scanner_extend_path_str = GLib.Path.build_path (Path.DIR_SEPARATOR_S,
															   "/usr", "share",
																SCANNER_EXTEND_FILE);

		string image_directory = GLib.Path.build_path (Path.DIR_SEPARATOR_S,
													   GLib.Environment.get_user_special_dir (GLib.UserDirectory.PICTURES));

        GLib.File scanner_file_config = GLib.File.new_for_path (scanner_path_str);
	    GLib.File scanner_file_extend = GLib.File.new_for_path (scanner_extend_path_str);

        images_dir = GLib.File.new_for_path (image_directory);

        output_dir = GLib.File.new_for_uri (output_directory);

		create_dir (scanner_file_config.get_parent ());
		create_dir (output_dir);

		delete_scanned_files (output_dir);

        try {
			monitor = output_dir.monitor (0);
			monitor.changed.connect (file_scanned_directory);
		} catch (Error e) {
			warning ("Could not monitor %s : %s", output_directory, e.message);
		}

		source_map = new Gee.HashMap<string, string> ();
		scanner_config = new GLib.KeyFile ();
        has_scanner = load_key_file (scanner_config, scanner_file_config);
        if (has_scanner)
			get_keys_config ();

		// The scanner extend is destined to user which has scanners with source and mode not referenced in Enums.vala
		scanner_extend = new GLib.KeyFile ();
		bool has_extend = load_key_file (scanner_extend, scanner_file_extend);
		if (!has_extend)
			warning ("Could not load scanner-extends at path %s", scanner_extend_path_str);
	}

	private void delete_scanned_files (GLib.File directory) {
		try {
			var enumerator = directory.enumerate_children (GLib.FileAttribute.STANDARD_NAME, GLib.FileQueryInfoFlags.NONE);
			var file_info = enumerator.next_file ();
			while (file_info != null) {
			var child = directory.get_child (file_info.get_name ());
				child.delete ();
				file_info = enumerator.next_file ();
				debug ("Removing %s from %s", child.get_basename (), directory.get_basename ());
			}
		} catch (Error e) {
			warning ("Could not delete files into %s : %s", directory.get_uri (), e.message);
		}

		if (scanned_files != null)
			scanned_files = null;
		if (pdf_file != null)
		    pdf_file = null;
	}

	private string get_content_type (GLib.File file) {
		GLib.FileInfo? info = null;
		string content_type = "";
		try {
			info = file.query_info (GLib.FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE);
		} catch (Error e) {
			warning ("Could not query info for file %s", file.get_uri ());
		}

		if (info != null)
			content_type = info.get_content_type ();
		return content_type;
	}

	private void file_scanned_directory (GLib.File _file, GLib.File? other_file, FileMonitorEvent event) {
		switch (event) {
			case GLib.FileMonitorEvent.CHANGES_DONE_HINT:
				string type = get_content_type (_file);
				if (type.contains ("image")) {
					if (scanned_files.find_custom (_file.get_path (), GLib.strcmp) == null)
						scanned_files.append (_file.get_path ());
				} else if (type.contains ("pdf")) {
				    pdf_file = _file;
				}
				break;
			default:
				break;
		}
	}

	private string? move_and_rename (string name, GLib.File destination, bool send_by_email=false) {
		string? new_file;
		string extension;

	    GLib.List <GLib.File> files = new GLib.List <GLib.File>();
	    if (pdf_file != null) {
                files.prepend (pdf_file);
                extension = ".pdf";
        } else {
			if (scanned_files.length () > 1) {
				warning ("The length of Scanned files is supposed to be 1.");
				return null;
			}
			GLib.File file = GLib.File.new_for_path (scanned_files.data);
			files.prepend (file);
			extension = ".jpeg";
	    }

		if (name != time_format_str)
			new_file = GLib.Path.build_path (Path.DIR_SEPARATOR_S, destination.get_path (), name + extension);
		else
			new_file = GLib.Path.build_path (Path.DIR_SEPARATOR_S, destination.get_path (), time_format_str + extension);

		GLib.File renamed_file = GLib.File.new_for_path (new_file);
	    string? num_duplicate = Utils.add_number_if_duplicate (renamed_file.get_uri (), extension);

		Files.FileOperations.copy_move_link.begin (files, destination, Gdk.DragAction.COPY, null, null, (res) => {
			string current_file;
			current_file = GLib.Path.build_path (Path.DIR_SEPARATOR_S, destination.get_path (), DEFAULT_SCAN_NAME + extension);
            GLib.File main_file = GLib.File.new_for_path (current_file);
            string new_name;
			if (num_duplicate != null)
                new_name = GLib.Filename.display_name (num_duplicate);
		    else if (name != time_format_str)
				new_name = name + extension;
            else
	            new_name = time_format_str + extension;

	        if (send_by_email) {
	            Files.FileUtils.set_file_display_name.begin (main_file, new_name, null, (obj, res) => {
					try {
						var file = Files.FileUtils.set_file_display_name.end (res);
						if (file != null) {
							string path = file.get_path ();
							if (pdf_file == null)
								Utils.rescale_image (path);
							SendTo.send_attachment (path);
						}
					} catch (Error e) {
						warning ("Could not send by email : %s", e.message);
					}
				});
            } else
	            Files.FileUtils.set_file_display_name.begin (main_file, new_name);
        });
        return num_duplicate != null ? num_duplicate : (name != time_format_str) ? name : time_format_str;
	}

	public string? save_scanned_files (string name) {
		GLib.return_if_fail (scanned_files != null || scanned_files.length () > 0);

		return move_and_rename (name, images_dir);
	}

	public void send_by_email (string name) {
        GLib.return_if_fail (scanned_files != null || scanned_files.length () > 0);

        string path_runtime = GLib.Path.build_filename (GLib.Environment.get_user_runtime_dir (), "pantheon-files");
        GLib.File runtime_file = GLib.File.new_for_path (path_runtime);
        create_dir (runtime_file);

		move_and_rename (name, runtime_file, true);
	}

	public async string generate_pdf () {
		return Utils.generate_pdf (scanned_files, DEFAULT_SCAN_NAME);
	}

	private void create_dir (GLib.File file) {
        try {
            if (!file.query_exists ()) {
                file.make_directory_with_parents ();
            }
        } catch (Error e) {
            warning ("Could not create directory %s : %s", file.get_uri (), e.message);
        }
	}

	private bool load_key_file (GLib.KeyFile keyfile, GLib.File scanner_file) {
		bool success = false;
		try {
			if (scanner_file.query_exists ())
				success = keyfile.load_from_file (scanner_file.get_path (), GLib.KeyFileFlags.NONE);
		} catch (Error e) {
			warning ("Could not load scanner config from %s : %s", scanner_file.get_path (), e.message);
		}
		return success;
	}

    private void get_keys_config () {
	    try {
		    this.device.uri = scanner_config.get_string ("Scanner", "uri");
			this.device.model = scanner_config.get_string ("Scanner", "model");
			this.device.modes = scanner_config.get_string_list ("Scanner", "modes");
			this.device.low_quality = scanner_config.get_integer ("Scanner", "low");
			this.device.high_quality = scanner_config.get_integer ("Scanner", "high");
			this.device.sources = scanner_config.get_string_list ("Scanner", "sources");
			this.device.resolutions = scanner_config.get_string_list ("Scanner", "resolutions");
		} catch (Error e) {
			warning ("Cannot get config file : %s", e.message);
		}
	}

    public void save_config () {
		scanner_config.set_string ("Scanner", "uri", this.device.uri);
		scanner_config.set_string ("Scanner", "model", this.device.model);
		scanner_config.set_string_list ("Scanner", "modes", this.device.modes);
		scanner_config.set_integer ("Scanner", "low", this.device.low_quality);
		scanner_config.set_integer ("Scanner", "high", this.device.high_quality);
		scanner_config.set_string_list ("Scanner", "sources", this.device.sources);
		scanner_config.set_string_list ("Scanner", "resolutions", this.device.resolutions);

		try {
			if (scanner_path_str != null)
				scanner_config.save_to_file (scanner_path_str);
		} catch (Error e) {
			warning ("Could not create file directory for config : %s", e.message);
		}
	}

    private GLib.Subprocess adf_subprocess (string script_name, string command, string mode, string resolution, string real_source, string output_dir) {
	    string username = GLib.Path.get_basename (PF.UserUtils.get_real_user_home ());
	    GLib.Subprocess subprocess = null;
		try {
			if (device.sources.length > 1) {
				subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE,
#if ELEMENTARY
                                                  "sudo",
                                                  script_name,
                                                  command,
                                                  username,
#else
                                                  script_name,
                                                  command,
#endif
                                                  this.device.uri,
                                                  "--source", real_source,
                                                  "--mode", mode,
                                                  "--resolution", resolution,
                                                  "--format", "jpeg",
                                                  "--batch=" + output_dir);
			} else {
				subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE,
#if !(ELEMENTARY)
                                                  "sudo",
                                                  script_name,
                                                  command,
                                                  username,
#else
                                                  script_name,
                                                  command,
#endif
                                                  this.device.uri,
                                                  "--mode", mode,
                                                  "--resolution", resolution,
                                                  "--format", "jpeg",
                                                  "--batch=" + output_dir);
			}
		} catch (Error e) {
			critical ("Could not start adf_subprocess for scannig : %s", e.message);
		}

		return subprocess;
    }

	private GLib.Subprocess flatbed_subprocess (string script_name, string command, string mode, string resolution, string real_source, string output_dir) {
		string username = GLib.Path.get_basename (PF.UserUtils.get_real_user_home ());
		GLib.Subprocess subprocess = null;
		try {
			if (device.sources.length > 1) {
				subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE,
#if !(ELEMENTARY)
                                                  "sudo",
                                                  script_name,
                                                  command,
                                                  username,
#else
                                                  script_name,
                                                  command,
#endif
                                                  this.device.uri,
                                                  "--source", real_source,
                                                  "--mode", mode,
                                                  "--resolution", resolution,
                                                  "--format", "jpeg",
                                                  "-o", output_dir);
			} else {
				subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE,
#if !(ELEMENTARY)
                                                  "sudo",
                                                  script_name,
                                                  command,
                                                  username,
#else
                                                  script_name,
                                                  command,
#endif
                                                  this.device.uri,
                                                  "--mode", mode,
                                                  "--resolution", resolution,
                                                  "--format", "jpeg",
                                                  "-o", output_dir);
			}
		} catch (Error e) {
			critical ("Could not start flatbed_subprocess for scannig : %s", e.message);
		}

		return subprocess;
	}

	public void scan_with_script (string script_name, string command, string color, uint resolution, string source) {
		GLib.Subprocess subprocess;

	    string new_file = DEFAULT_SCAN_NAME;

		string mode = color;//get_mode_translated (color);
		string resolution_str = resolution.to_string ();
		string real_source = source_map.@get(source);
		string output_scanned_file;

		delete_scanned_files (output_dir);

		if (source != _(PLATEN_TRANSLATED)) {
			set_pdf = true;
			new_file += " %d.jpeg";
			output_scanned_file = GLib.Path.build_path (Path.DIR_SEPARATOR_S, output_dir.get_path (), new_file);
			subprocess = adf_subprocess (script_name, command, mode, resolution_str, real_source, output_scanned_file);
		} else {
			set_pdf = false;
			new_file += ".jpeg";
			output_scanned_file = GLib.Path.build_path (Path.DIR_SEPARATOR_S, output_dir.get_path (), new_file);
			subprocess = flatbed_subprocess (script_name, command, mode, resolution_str, real_source, output_scanned_file);
		}

		subprocess.wait_async.@begin (null, (res) => {
			// We add a timeout to leave time for files to be image/jpeg
			GLib.Timeout.add (500, () => {
				if (!has_gray) {
					scanned_files.@foreach ( (file) => {
						Utils.convert_color_to_grayscale (file);
					});
				}
				scan_finished (scanned_files);
				return GLib.Source.REMOVE;
			});
		});
        is_scanning = true;
		return;
	}

	public void set_modes_from_list (GLib.SList<string> list_modes) {
		GLib.return_if_fail (list_modes != null);
		GLib.return_if_fail (list_modes.length () != 0);

		device.modes = new string [list_modes.length ()];

		int i = 0;
		foreach (var mode in list_modes) {
			device.modes[i] = mode.dup ();
			i++;
		}
	}

	public void set_resolutions_from_list (GLib.SList<string> list_resolutions) {
		GLib.return_if_fail (list_resolutions != null);
		GLib.return_if_fail (list_resolutions.length () != 0);

#if ELEMENTARY
		int i = 0;
		device.resolutions = new string [list_resolutions.length ()];
		device.low_quality = int.parse (list_resolutions.data);
		device.high_quality = int.parse (list_resolutions.last ().data);
		foreach (var it in list_resolutions) {
			device.resolutions[i] = it.dup ();
			i++;
		}
#else
		device.low_quality = int.parse (list_resolutions.data);
		if (device.low_quality < 100) {
			foreach (var it in list_resolutions) {
				if (int.parse (it) == 100) {
					device.low_quality = int.parse (it);
					break;
				} else if (int.parse (it) > 100) {
					device.low_quality = int.parse (it);
					break;
				}
			}
		}

		device.high_quality = int.parse (list_resolutions.last ().data);
		if (device.high_quality > 600) {
			list_resolutions.reverse ();
			foreach (var it in list_resolutions) {
				device.high_quality = int.parse (it);
				if (int.parse (it) <= 600)
					break;
			}
		}
#endif
	}

	public void set_sources_from_list (GLib.SList<string> list_sources) {
		GLib.return_if_fail (list_sources != null);
		GLib.return_if_fail (list_sources.length () != 0);

		device.sources = new string [list_sources.length ()];

		int i = 0;
		foreach (var source in list_sources) {
			device.sources[i] = source.dup ();
			i++;
		}
	}

	public string get_mode_translated (string color_checked) {
        string mode_found = null;

		switch (color_checked) {
#if ELEMENTARY
			case Scanner.MONOCHROME:
			case Scanner.LINEART:
				mode_found = _(LINEART_MODE);
				break;
#endif
			case Scanner.24BIT_COLOR_FAST:
			case Scanner.COLOR:
				mode_found = _(COLOR_MODE);
				break;
			case Scanner.TRUE_GRAY:
			case Scanner.GRAY:
			case Scanner.GRAYSCALE:
				mode_found = _(GRAYSCALE_MODE);
				break;
			default:
				mode_found = get_extended_mode (color_checked);
				break;
		}

		return mode_found;
	}

	public string get_source_translated (string source_str) {
	    string source_translated = "";

	    switch (source_str) {
	        case Scanner.PLATEN:
	        case Scanner.FLATBED:
	        case Scanner.FLATBED_2:
	        case Scanner.DOCUMENT_TABLE:
		        source_translated = _(PLATEN_TRANSLATED);
		        break;
	        case Scanner.ADF:
	        case Scanner.ADF_LEFT:
	            source_translated = _(ADF_TRANSLATED);
	            break;
	        case Scanner.ADF_DUPLEX:
	        case Scanner.ADF_CENTER:
	            source_translated = _(ADF_DUPLEX_TRANSLATED);
	            break;
	        default:
				source_translated = get_extended_source (source_str);
				break;
	    }

		source_map.@set (source_translated, source_str);
		return source_translated;
	}

	private string get_extended_source (string source_str) {
		string founded_source = "";
		try {
			var simplex = scanner_extend.get_string ("ScannerExtends", "simplex");
			if (simplex == source_str)
				founded_source = _(PLATEN_TRANSLATED);

			var adf_extend = scanner_extend.get_string ("ScannerExtends", "adf");
			if (adf_extend == source_str)
				founded_source = _(ADF_TRANSLATED);

			var duplex_extend = scanner_extend.get_string ("ScannerExtends", "duplex");
			if (duplex_extend == source_str)
				founded_source = _(ADF_DUPLEX_TRANSLATED);
		} catch (Error e) {
			warning ("Could not retrieve extended source %s", e.message);
		}

		return founded_source;
	}

	private string get_extended_mode (string mode) {
		string mode_found = null;
		try {
			var color_extend = scanner_extend.get_string ("ScannerExtends", "color");
			if (color_extend == mode)
				mode_found = mode;

			var gray_extend = scanner_extend.get_string ("ScannerExtends", "gray");
			if (gray_extend == mode)
				mode_found = mode;
		} catch (Error e) {
			warning ("Could not retrieve extended modes %s", e.message);
		}
		return mode_found;
	}
}
