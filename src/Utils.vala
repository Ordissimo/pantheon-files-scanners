/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 namespace Utils {
    private void error_handler (HPDF.Status error_no, HPDF.Status detail_no) {
        error ("Error(%04x) %d - detail %d\n", (uint)error_no, (int)error_no, (int)detail_no);
    }

    public string? add_number_if_duplicate (string _path, string? set_extension = null) {
        bool name_changed = false;
        int end_offset;

        string path = _path;

        string basename_ext = GLib.Path.get_basename (path);

		Files.FileUtils.get_rename_region (basename_ext, null, out end_offset, false);

		string basename = basename_ext.substring (0, end_offset);
        string extension;
        if (set_extension != null)
            extension = set_extension;
        else
            extension = basename_ext.substring (end_offset, -1);

        GLib.File file;
        file = GLib.File.new_for_uri (path);
        GLib.File parent = file.get_parent ();

        int i = 0;
		/* Check if filename exists and add prefix if it is.
		 * Not the best way but despite adding the prefix automatically
		 * a dialog error appear.
		*/
		while (file.query_exists ()) {
		    if (!name_changed)
		        name_changed = true;
			i++;
			string new_dest = GLib.Path.build_path (Path.DIR_SEPARATOR_S, parent.get_uri (), basename + " " + i.to_string () + extension);
			file = GLib.File.new_for_uri (new_dest);
		}

		basename = file.get_basename ();
		return name_changed ? basename : null;
    }

    public string generate_pdf (GLib.SList<string> images, string output_name) {
        HPDF.Doc pdf = new HPDF.Doc(error_handler);
        pdf.set_compression_mode (HPDF.COMP_ALL);

        string parent_dir = GLib.Path.get_dirname (images.data);
        string filename = GLib.Path.build_path (Path.DIR_SEPARATOR_S, parent_dir, output_name + ".pdf");

        foreach (var image_uri in images) {
            unowned HPDF.Page page = pdf.add_page ();

            unowned HPDF.Image img = pdf.load_jpeg_image_from_file (image_uri);
            page.set_width (img.get_width ());
            page.set_height (img.get_height ());
            page.draw_image (img, 0, 0, img.get_width (), img.get_height ());
        }

        /* save the document to a file */
        debug ("Writing pdf to: %s\n", filename);
        pdf.save_to_file (filename);

        return filename;
    }

    private Gdk.Pixbuf? get_pixbuf_from_file (string filename) {
         Gdk.Pixbuf pixbuf = null;

        try {
            pixbuf = new Gdk.Pixbuf.from_file (filename);
        } catch (Error e) {
            warning ("Could not load %s into pixbuf : %s", filename, e.message);
        }
        return pixbuf;
    }

    private void save_to_file (Gdk.Pixbuf pixbuf, string destination) {
        try {
            pixbuf.save (destination, "jpeg", "quality", "100");
        } catch (Error e) {
            warning ("Could not save %s : %s", destination, e.message);
        }
    }

    public void convert_color_to_grayscale (string filename) {
        GLib.return_if_fail (filename != null);
        GLib.return_if_fail (filename != "");

        Gdk.Pixbuf pixbuf = get_pixbuf_from_file (filename);

        if (pixbuf == null)
            return;

        uint bpp = pixbuf.has_alpha ? 4 : 3;
        uint size = pixbuf.height * pixbuf.width * bpp;
        unowned uchar *pixels = (uchar*)pixbuf.pixels;

        uint i;
        for (i = 0; i < size; i += bpp) {
            // Set pixel color to green with a transparency of 128
            double r = (double)pixels[i + 2];
            double g = (double)pixels[i + 1];
            double b = (double)pixels[i + 0];
            double rn = r * 0.3 + g * 0.59 + b * 0.11;
            double gn = r * 0.3 + g * 0.59 + b * 0.11;
            double bn = r * 0.3 + g * 0.59 + b * 0.11;
            pixels[i + 2] = (uchar)rn;
            pixels[i + 1] = (uchar)gn;
            pixels[i + 0] = (uchar)bn;
        }

        save_to_file (pixbuf, filename);
    }

    public void rescale_image (string filename) {
        GLib.return_if_fail (filename != null);
        GLib.return_if_fail (filename != "");

        Gdk.Pixbuf pixbuf = get_pixbuf_from_file (filename);
        double width = pixbuf.get_width ();
        double height = pixbuf.get_height ();

        if (width < 1000 || height < 1000)
            return;

        debug ("Reducing size (%lf, %lf) to (%lf, %lf) for file %s", width, height, width*0.25, height*0.25, filename);

        width *= 0.25;
        height *= 0.25;

        pixbuf = pixbuf.scale_simple((int)width, (int)height, Gdk.InterpType.BILINEAR);
        save_to_file (pixbuf, filename);
    }
}
