/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


 namespace Scanner {

    /*
     * MODE
     */

    public const string MONOCHROME = "Monochrome";
    public const string LINEART = "Lineart";


    public const string 24BIT_COLOR_FAST = "24bit Color[Fast]";
    public const string COLOR = "Color";

    public const string TRUE_GRAY = "True Gray";
    public const string GRAY = "Gray";
    public const string GRAYSCALE = "Grayscale";


    /*
     * SOURCES
     */
    public const string PLATEN = "Platen";
    public const string FLATBED = "Flatbed";
    public const string FLATBED_2 = "FlatBed";
    public const string DOCUMENT_TABLE = "Document Table";

    public const string ADF = "ADF";
    public const string ADF_LEFT = "Automatic Document Feeder(left aligned)";

    public const string ADF_DUPLEX = "ADF Duplex";
    public const string ADF_CENTER = "Automatic Document Feeder(centrally aligned)";
}
