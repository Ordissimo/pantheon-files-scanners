/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class BoxOptions: Gtk.Box {
    //protected Gtk.Orientation orientation { get; set; default=Gtk.Orientation.VERTICAL; }

    private ScannerDevice scanner_device;

    //private const string LINEART_MODE = N_("Lineart");
    //private const string GRAYSCALE_MODE = N_("Gray");
    //private const string COLOR_MODE = N_("Color");

    private const string LOW_QUALITY = N_("Low quality");
    private const string HIGH_QUALITY = N_("High quality");

    private Gtk.ButtonBox buttons_mode;
#if ELEMENTARY
    private Gtk.Box box_ranges;
#else
    private Gtk.ButtonBox buttons_resolution;
#endif
    private Gtk.ButtonBox buttons_source;

    // Options checked
	public string color_checked { get; set; default = ""; }
	public bool lineart_checked { get; set; default = false; }
	public uint resolution_checked { get; set; }
	public string source_checked { get; set; default = ""; }

	construct {
		this.orientation = Gtk.Orientation.VERTICAL;
		this.halign = Gtk.Align.START;
		this.valign = Gtk.Align.FILL;
		this.margin_start = 20;
	}

    public BoxOptions (ScannerDevice device) {
        this.scanner_device = device;

		buttons_mode = new Gtk.ButtonBox (Gtk.Orientation.VERTICAL) {
			layout_style=Gtk.ButtonBoxStyle.EXPAND
		};

		buttons_source = new Gtk.ButtonBox (Gtk.Orientation.VERTICAL) {
			layout_style=Gtk.ButtonBoxStyle.EXPAND
		};

		add_modes ();
#if ELEMENTARY
        add_resolutions_range ();
#else
		buttons_resolution = new Gtk.ButtonBox (Gtk.Orientation.VERTICAL) {
			layout_style=Gtk.ButtonBoxStyle.EXPAND
		};
        add_resolutions ();
#endif
        add_sources ();
	}

	private bool on_enter_notify_buttonsbox (Gtk.Widget widget, Gdk.EventCrossing event) {
		Gdk.Cursor pointer = new Gdk.Cursor.from_name (Gdk.Display.get_default(), "pointer");
		((Gdk.Window)widget.get_parent_window()).set_cursor (pointer);
		return true;
	}

	private bool on_leave_notify_buttonsbox (Gtk.Widget widget, Gdk.EventCrossing event) {
		((Gdk.Window)widget.get_parent_window()).set_cursor (null);
		return true;
	}

    private void add_modes () {
		if (buttons_mode.get_children () != null)
			return;

		Gtk.RadioButton button_1 = null;
		foreach (string mode in scanner_device.device.modes) {
			string mode_translated = scanner_device.get_mode_translated (mode);
			if (mode_translated == null)
				continue;

			if (button_1 == null) {
				button_1 = new Gtk.RadioButton.with_label (null, mode_translated);
				button_1.get_style_context ().add_class ("color-radio");
				button_1.button_press_event.connect (mode_checked_cb);

				button_1.enter_notify_event.connect (on_enter_notify_buttonsbox);
				button_1.leave_notify_event.connect (on_leave_notify_buttonsbox);

				buttons_mode.add (button_1);
			} else {
				Gtk.RadioButton button = null;
				button = new Gtk.RadioButton.with_label_from_widget (button_1, mode_translated);
				button.get_style_context ().add_class ("color-radio");
				button.button_press_event.connect (mode_checked_cb);

				button.enter_notify_event.connect (on_enter_notify_buttonsbox);
				button.leave_notify_event.connect (on_leave_notify_buttonsbox);

				buttons_mode.add (button);
			}
		}

		button_1.set_active (true);
        this.color_checked = button_1.label;

		this.pack_start (buttons_mode, true, false);
	}

#if !(ELEMENTARY)
    private void add_resolutions () {
		if (buttons_resolution.get_children () != null)
			return;

		Gtk.RadioButton button_1, button_2;

		button_1 = new Gtk.RadioButton.with_label (null, _(LOW_QUALITY));
		button_1.get_style_context ().add_class ("color-radio");
		button_1.button_press_event.connect (resolution_checked_cb);
		buttons_resolution.add (button_1);

		button_2 = new Gtk.RadioButton.with_label_from_widget (button_1, _(HIGH_QUALITY));
		button_2.get_style_context ().add_class ("color-radio");
		button_2.button_press_event.connect (resolution_checked_cb);
		buttons_resolution.add (button_2);

		// Change cursor hover button
		button_1.enter_notify_event.connect (on_enter_notify_buttonsbox);
		button_1.leave_notify_event.connect (on_leave_notify_buttonsbox);

		button_2.enter_notify_event.connect (on_enter_notify_buttonsbox);
		button_2.leave_notify_event.connect (on_leave_notify_buttonsbox);

		// Default Resolution;
		this.resolution_checked = scanner_device.device.low_quality;

		this.pack_start (buttons_resolution, true, false);
	}
#else
	private Gtk.Widget create_combobox_text () {
		var resolutions_range = new Gtk.ComboBoxText ();
		foreach (var resolution in scanner_device.device.resolutions)
		    resolutions_range.append (null, resolution);

		resolutions_range.set_active (0);
		resolutions_range.changed.connect ( () => {
			resolution_checked = int.parse (resolutions_range.get_active_text ());
		});
		return resolutions_range;
	}

	private Gtk.Widget create_range_scale () {
		var resolutions_range = new Gtk.Scale.with_range (Gtk.Orientation.HORIZONTAL, scanner_device.device.low_quality, scanner_device.device.high_quality, 25) {
			value_pos=Gtk.PositionType.RIGHT,
			width_request=300
		};
		resolutions_range.change_value.connect ( (scroll, new_value) => {
			if ( (uint)new_value >= scanner_device.device.high_quality)
				resolution_checked = scanner_device.device.high_quality;
			else
				resolution_checked = (uint)new_value;
			return false;
		});
		return resolutions_range;
	}

    public void add_resolutions_range () {
		if (box_ranges != null && box_ranges.get_parent () != null) {
			box_ranges.get_children ().last ().data.destroy ();
		} else {
			box_ranges = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 50);
			var label = new Gtk.Label (_("Quality"));
			label.get_style_context ().add_class (Granite.STYLE_CLASS_H3_LABEL);
			box_ranges.add (label);
			this.pack_start (box_ranges, true, false);
		}


		if (scanner_device.device.resolutions.length > 2)
			box_ranges.add (create_combobox_text ());
		else
			box_ranges.pack_start (create_range_scale (), true, true);

		// Default Resolution;
		this.resolution_checked = scanner_device.device.low_quality;
	}
#endif

	private void add_sources () {
		// If only 1 source no need to add buttonbox
		if (scanner_device.device.sources.length == 1) {
			this.source_checked = scanner_device.get_source_translated (scanner_device.device.sources [0]);
			return;
		}

		Gtk.RadioButton button_1 = null;

		foreach (string source in scanner_device.device.sources) {
			if (button_1 == null) {
				button_1 = new Gtk.RadioButton.with_label (null, scanner_device.get_source_translated (source));
				button_1.get_style_context ().add_class ("color-radio");
				button_1.button_press_event.connect (source_checked_cb);

				button_1.enter_notify_event.connect (on_enter_notify_buttonsbox);
				button_1.leave_notify_event.connect (on_leave_notify_buttonsbox);

				buttons_source.add (button_1);
			} else {
				Gtk.RadioButton button = null;
				button = new Gtk.RadioButton.with_label_from_widget (button_1, scanner_device.get_source_translated (source));
				button.get_style_context ().add_class ("color-radio");
				button.button_press_event.connect (source_checked_cb);

				button.enter_notify_event.connect (on_enter_notify_buttonsbox);
				button.leave_notify_event.connect (on_leave_notify_buttonsbox);

				buttons_source.add (button);
			}
		}

		this.source_checked = button_1.label;
		this.pack_start (buttons_source, true, false);
	}

	public void update_sources () {
	    var childrens = buttons_source.get_children ();
		if (childrens != null) {
			childrens.@foreach ( (child) => {
				child.destroy ();
			});
		}
		this.remove (buttons_source);
	    add_sources ();
	}

	/*
	 * SIGNALS
	 */
#if !(ELEMENTARY)
	private bool resolution_checked_cb (Gtk.Widget widget, Gdk.EventButton event) {
		Gtk.RadioButton button = widget as Gtk.RadioButton;
		if (button.get_active ())
			return true;

		if (button.label == _(LOW_QUALITY))
			this.resolution_checked = scanner_device.device.low_quality;
		else if (button.label == _(HIGH_QUALITY))
			this.resolution_checked = scanner_device.device.high_quality;
		return false;
	}
#endif

	private bool mode_checked_cb (Gtk.Widget widget, Gdk.EventButton event) {
		Gtk.RadioButton button = widget as Gtk.RadioButton;
		if (button.get_active ())
			return true;

		if (button.label == _(scanner_device.LINEART_MODE))
			this.color_checked = scanner_device.LINEART_MODE;
		else if (button.label == _(scanner_device.GRAYSCALE_MODE))
			this.color_checked = scanner_device.GRAYSCALE_MODE;
		else if (button.label == _(scanner_device.COLOR_MODE))
			this.color_checked = scanner_device.COLOR_MODE;
		return false;
	}

	private bool source_checked_cb (Gtk.Widget widget, Gdk.EventButton event) {
		Gtk.RadioButton button = widget as Gtk.RadioButton;
		if (button.get_active ())
			return true;

		this.source_checked = button.label;
		return false;
	}
}
