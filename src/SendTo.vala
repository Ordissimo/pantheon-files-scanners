/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class SendTo {
    enum Mailer {
        UNKNOWN,
        CUSTOM,
        BALSA,
        EVOLUTION,
        PANTHEON_MAIL,
        SYLPHEED,
        THUNDERBIRD
    }

    private static string mail_cmd;
    private static Mailer mail_type;
    private static string mailto = null;

    private static Settings settings = null;

    private static string? get_evolution_cmd () {
        string tmp, retval;

        tmp = GLib.Environment.find_program_in_path ("evolution");
        if (tmp == null)
            return null;

        retval = ("%s --component %%s").printf (tmp);
        return retval;
    }

    public static void send_attachment (string filename) {
        if (mailto == null) {
            mail_cmd = find_default_email ();

            if (mail_cmd == null)
                return;
        }

        get_mailto (filename);

        string cmd;
        if (mail_type != Mailer.PANTHEON_MAIL)
            cmd = mail_cmd.printf (mailto);
        else
            cmd = mailto;

        send (cmd);
        debug ("Sending email with command %s", cmd);
    }

    private static void send (string cmd) {
        try {
            GLib.Process.spawn_command_line_async (cmd);
        } catch (Error e) {
            warning ("Could not spawn %s : %s", mail_cmd, e.message);
        }
    }

    private static string find_default_email () {
        string cmd;
        var app_info = GLib.AppInfo.get_default_for_uri_scheme ("mailto");
        if (app_info != null) {
            cmd = app_info.get_commandline ().dup ();
        } else
            cmd = null;

        settings = new Settings ("io.elementary.files.email");

        if (settings != null && settings.get_string ("application-name") != "") {
            mail_type = Mailer.CUSTOM;
            cmd = settings.get_string ("application-name");
            debug ("Using custom command %s", cmd);
        } else if (cmd == null || cmd[0] == '\0') {
            cmd = get_evolution_cmd ();
            mail_type = Mailer.EVOLUTION;
        } else {
            if ("balsa" in cmd) {
                mail_type = Mailer.BALSA;
            } else if ("thunder" in cmd || "seamonkey" in cmd || "icedove" in cmd) {
                mail_type = Mailer.THUNDERBIRD;

                string[] strv = cmd.split (" ");
                cmd = ("%s %%s").printf (strv[0]);
            } else if ("sylpheed" in cmd || "claws" in cmd) {
                mail_type = Mailer.SYLPHEED;
            } else if ("evolution" in cmd || "anjal" in cmd) {
                mail_type = Mailer.EVOLUTION;
            } else if ("pantheon-mail" in cmd)
                mail_type = Mailer.PANTHEON_MAIL;
        }

        if (cmd != null) {
            cmd = cmd.replace ("%U", "%s");
            cmd = cmd.replace ("%u", "%s");
        }
        return cmd;
    }

    private static void get_mailto (string filename) {
        mailto = "";
        switch (mail_type) {
            case Mailer.BALSA:
                get_balsa_mailto (filename);
                break;
            case Mailer.CUSTOM:
                get_custom_mailto (filename);
                break;
            case Mailer.SYLPHEED:
                get_sylpheed_mailto (filename);
                break;
            case Mailer.THUNDERBIRD:
                get_thunderbird_mailto (filename);
                break;
            case Mailer.UNKNOWN:
                critical ("Didn't found the default email %s", mail_cmd);
                break;
            case Mailer.EVOLUTION:
                get_evolution_mailto (filename);
                break;
            case Mailer.PANTHEON_MAIL:
            default:
                get_pantheon_mailto (filename);
                break;
        }
    }

    private static void get_evolution_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        mailto += "mailto:\"\"?attach=\"%s\"".printf (filename);
    }

    private static void get_custom_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        mailto += settings.get_string ("command-attachment").printf (filename);
    }

    private static void get_balsa_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        if ( (" -m " in mail_cmd) == false || (" --compose=" in mail_cmd) == false) {
            mailto += " --compose=";
        }
        mailto += ("\"\" --attach=\"%s\"").printf (filename);
    }

    private static void get_thunderbird_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        mailto += ("-compose \"attachment=\'%s\'\"").printf (filename);
    }

    private static void get_sylpheed_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        mailto += "--compose \"\"--attach \"%s\"".printf (filename);
    }

    private static void get_pantheon_mailto (string filename) {
        return_if_fail (filename != null || filename == "");

        mailto += "mail-attach \"%s\"".printf (filename);
    }
}
