/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 public class Files.Plugins.Scanners : Files.Plugins.Base {
	private Files.SidebarInterface sidebar;

	private Files.AbstractSlot slot_view;
	private Gtk.Overlay slot_overlay;

    private Gtk.Box box_scan; // Display white background

    // ProgressBar overlay
    private Gtk.Box progress_info; // Searching for scanner and scan.
    private Gtk.Label label_progress_info;
    private Gtk.ProgressBar progress;

    // Label info + List scanners
    private Gtk.Box box_info; // Display when no scanners found or multiple scanners found
    private Gtk.Label label_info;
    private Gtk.IconView scanners_iconview; // Displayed when multiple scanners found.
    private Gtk.ListStore scanners_model;

    // Options scanner and output
    private Gtk.Box hbox;
    private BoxOptions options_scanner;
    private BoxOutputScanner previewer_scanner;

    private Gtk.Button search_scanner;
    private Gtk.Button button_scan;

    private Gtk.Box box_bottom_actions;
    private Gtk.ButtonBox buttonbox_actions_right; // Send by email + save

    private ScannerDevice scanner_device;

    private static string scanner_output;

    private uint pulse_id;

    private bool is_running;
    private StateUI state;

#if ELEMENTARY
	private const string SCANNER_EXEC = "scanimage";
	private const string SCANNER_FIND = "-f %d|%m%n";
	private const string SCANNER_DETAILS = "-A";
	private const string SCANNER_SCAN = "-d";
#else
	private const string SCANNER_EXEC = "/usr/share/sfiler/scripts/scanner-script";
	private const string SCANNER_FIND = "find";
	private const string SCANNER_DETAILS = "details";
	private const string SCANNER_SCAN = "scan";
#endif

    private const string SCANNER = N_("Scanner");
    private const string SCANNER_OUTPUT_DIR = "pantheon-files/scanner-output";

	private const string SEARCH_SCANNERS = N_("Search for scanners in progress...");
	private const string SCANNERS_NOT_FOUND = N_("No scanner found...");
	private const string SAVED_SCANNER_NOT_FOUND = N_("The scanner %s were not found.\n Please make sure that it is turned on, wifi is enabled or plugged into USB.");

	private const string LABEL_CHOOSE = N_("Please select the scanner you would like to use.");
	private const string CONFIGURATION_IN_PROGRESS = N_("%s scanner configuration in progress...");
	private const string SCAN_IN_PROGRESS = N_("Scanning in progress...");
	private const string LABEL_SEARCH_SCANNERS = N_("Search for scanners again");
	private const string LABEL_ASSEMBLING_IMAGES = N_("Assembling %ld images into pdf...");
	private const string LABEL_SCAN = N_("Scanner");

	private const string LABEL_FILE_SAVED = N_("The file \"%s\" has been placed in Pictures.");

	private const string PILOTE = N_("Pilote : ");

    private enum ScanImageType {
        FIND_SCANNERS,
        OPTIONS_SCANNER,
        DEVICE_SCAN,
    }

	private enum ScannerInfo {
		ICON,
        URI,
        MODEL,
        PILOTE
    }

    private enum StateUI {
        SEARCHING_SCANNER,
        ERROR_FOUND,
        SCANNERS_FOUND,
        CONFIGURE_SCANNER,
        SCANNER_CONFIGURED,
        SCANNING,
        ASSEMBLING_IMAGES,
        IDLE
    }

    public Scanners () {
        is_running = false;

        slot_overlay = null;

        box_scan = null;
        box_info = null;

        scanner_device = null;
        scanners_iconview = null;
        scanners_model = null;
        scanner_output = GLib.Path.build_path (Path.DIR_SEPARATOR_S,
                                               GLib.Environment.get_user_cache_dir (),
                                               SCANNER_OUTPUT_DIR);

        try {
            GLib.File file = GLib.File.new_for_path (scanner_output);
            if (!file.query_exists ())
                file.make_directory_with_parents ();
            scanner_output = file.get_uri ();
        } catch (Error e) {
            warning ("Could not create directory %s : %s", scanner_output, e.message);
        }

        pulse_id = 0;
    }

    // Just a wrapper to add try and catch;
    private void launch_subprocess (ScanImageType type, string? command=null, string? uri=null) {
        try {
            subprocess_scanimage (type, command, uri);
        } catch (Error e) {
            critical ("Could not start subprocess with %s : %s", command != null ? command : "", e.message);
        }
    }

    private bool research_scanners_cb () {
        is_running = true;
        set_visible_boxes (false);
        search_scanner.set_visible (false);
        launch_subprocess (ScanImageType.FIND_SCANNERS, SCANNER_FIND);
        return false;
    }

    // Display information or list of scanners found.
    private void display_label_info (string? label, StateUI new_state) {
        if (box_info == null) {
            box_info = new Gtk.Box (Gtk.Orientation.VERTICAL, 0) {
                valign=Gtk.Align.CENTER
            };
            label_info = new Gtk.Label (label) {
                justify = Gtk.Justification.CENTER
            };
            label_info.get_style_context ().add_class (Granite.STYLE_CLASS_H1_LABEL);
            box_info.add (label_info);

            box_scan.pack_start (box_info, true, true);
        } else if (label != null)
            label_info.label = label;

        if (scanners_iconview != null && scanners_iconview.get_parent () == null)
            box_info.pack_start (scanners_iconview, true, true);

        box_info.show_all ();
        if (!box_bottom_actions.get_visible ()) {
            box_bottom_actions.show_all ();
        }

        state = new_state;
        if (progress_info != null && progress_info.get_visible ())
//            progress_info.set_visible (false);
            hide_progress ();
    }

    // Display progressbar when searching, configuring, and scanning.
    private void display_progressbar (string? label, StateUI new_state) {
        if (progress_info == null) {
            progress_info = new Gtk.Box (Gtk.Orientation.VERTICAL, 50) {
                valign=Gtk.Align.CENTER
            };
            progress_info.get_style_context ().add_class (Gtk.STYLE_CLASS_BACKGROUND);
            label_progress_info = new Gtk.Label (label) {
                justify = Gtk.Justification.CENTER
            };
            label_progress_info.get_style_context ().add_class (Granite.STYLE_CLASS_H1_LABEL);
            progress = new Gtk.ProgressBar () {
                height_request=30,
                margin_start=10,
                margin_end=10
            };

            progress_info.add (label_progress_info);
            progress_info.add (progress);

            slot_overlay.add_overlay (progress_info);
        } else if (label != null)
            label_progress_info.label = label;

        state = new_state;
        if (box_info != null && box_info.get_visible ())
            box_info.set_visible (false);

        if (box_bottom_actions.get_visible ())
            box_bottom_actions.set_visible (false);

        set_pulse_progressbar ();
        progress_info.show_all ();
    }

    private void set_pulse_progressbar () {
        if (pulse_id > 0) {
            message ("Pulse id != 0");
            return;
        }

        pulse_id = GLib.Timeout.add (50, () => {
				progress.pulse ();
				return GLib.Source.CONTINUE;
		});
    }

    private void hide_progress () {
        if (pulse_id > 0) {
            GLib.Source.remove (pulse_id);
            pulse_id = 0;
        }

        if (progress_info.get_visible ()) {
            GLib.Idle.add ( () => {
                progress_info.set_visible (false);
                return GLib.Source.REMOVE;
            });
        }
    }

    private void request_sidebar_update () {
        return_if_fail (sidebar != null);
        if (sidebar == null)
            return;

        sidebar.reload ();
    }

	public override void sidebar_loaded (Gtk.Widget widget) {
        sidebar = widget as Files.SidebarInterface;
        sidebar.add_plugin_item (adapt_plugin_item (),
                                 Files.PlaceType.STORAGE_CATEGORY);
        request_sidebar_update ();
    }

    public override void update_sidebar (Gtk.Widget widget) {
        sidebar.add_plugin_item (adapt_plugin_item (),
                                 Files.PlaceType.STORAGE_CATEGORY);
	}

	static Files.SidebarPluginItem adapt_plugin_item () {
		var item = new Files.SidebarPluginItem () {
			name = _(SCANNER),
			index = 0,
			icon = new ThemedIcon.with_default_fallbacks ("printer"),
			uri = scanner_output
        };
        return item;
	}

    private void display_bg_box () {
		box_scan = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        // background to hide the folder;
		box_scan.get_style_context ().add_class (Gtk.STYLE_CLASS_BACKGROUND);

        search_scanner = new Gtk.Button.with_label (_(LABEL_SEARCH_SCANNERS)) {
            halign=Gtk.Align.START,
            margin_top=10,
            margin_start=10,
            width_request=300
        };
        box_scan.pack_start (search_scanner, false, false);

        hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        box_scan.pack_start (hbox, true, true);

        box_bottom_actions = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0) {
            height_request=170
        };
        box_scan.pack_end (box_bottom_actions, false, false);

		slot_overlay.add_overlay (box_scan);
		box_scan.show_all ();

        search_scanner.button_press_event.connect (research_scanners_cb);
#if !(ELEMENTARY)
        search_scanner.get_style_context ().add_class ("circular");
#endif


		search_scanner.enter_notify_event.connect (on_enter_notify_button);
		search_scanner.leave_notify_event.connect (on_leave_notify_button);

        if (scanner_device == null) {
            scanner_device = new ScannerDevice (scanner_output);
        }

		if (!scanner_device.has_scanner)
		    search_scanner.set_visible (false);
    }

    public override void directory_loaded (Gtk.ApplicationWindow window, Files.AbstractSlot view, Files.File directory) {
		slot_view = view;

		if (view.uri == scanner_output){
		    if (slot_overlay == null) {
                slot_overlay = slot_view.overlay;
                display_bg_box ();
            }

            if (!scanner_device.has_scanner) {
                if (!is_running) {
                    is_running = true;
                    launch_subprocess (ScanImageType.FIND_SCANNERS, SCANNER_FIND);
                } else {
                    display_ui ();
                }
            } else {
                if (!is_running) {
                    if (progress_info != null) {
                        progress_info.set_visible (false);
                        hide_progress ();
                    }
                    configure_options ();
                }else
                    display_ui ();
            }
		} else {
		    if (box_scan != null && box_scan.get_visible ()) {
		        if (!is_running) {
		            if (progress_info != null) {
                        progress_info.destroy ();
                        progress_info = null;
                    }
                    box_scan.destroy ();
                    box_scan = null;

                    slot_overlay = null;
                    options_scanner = null;
                } else {
                    hide_ui ();
		        }
		    }
		}
	}

    // Hide options, previewer, and actions
    private void set_visible_boxes (bool set_visible) {
        if (set_visible)
            box_bottom_actions.show_all ();
        else
            box_bottom_actions.set_visible (set_visible);
        search_scanner.set_visible (set_visible);
        if (options_scanner != null)
            options_scanner.set_visible (set_visible);
        if (button_scan != null)
            button_scan.set_visible (set_visible);
        if (previewer_scanner != null)
            previewer_scanner.set_visible (set_visible);
    }

    private void display_ui () {
        box_scan.set_visible (true);
        switch (state) {
            case StateUI.SEARCHING_SCANNER:
            case StateUI.CONFIGURE_SCANNER:
            case StateUI.SCANNING:
                if (box_info != null)
                    box_info.set_visible (false);
                set_visible_boxes (false);
                display_progressbar (null, state);
                break;
            case StateUI.ERROR_FOUND:
            case StateUI.SCANNERS_FOUND:
                display_label_info (null, state);
                set_visible_boxes (false);
                search_scanner.set_visible (true);
                progress_info.set_visible (false);
                hide_progress ();
                break;
            case StateUI.SCANNER_CONFIGURED:
                options_scanner.set_visible (true);
                search_scanner.set_visible (true);
                progress_info.set_visible (false);
                hide_progress ();
                break;
            case StateUI.IDLE:
                progress_info.set_visible (false);
                hide_progress ();
                set_visible_boxes (true);
                break;
            default:
                warning ("State %s not found", state.to_string ());
                break;
        }
    }

	private void hide_ui () {
	    if (box_scan != null && box_scan.get_visible ())
            box_scan.set_visible (false);
	    if (progress_info != null && progress_info.get_visible ()) {
            progress_info.set_visible (false);
            hide_progress ();
	    } if (box_info != null && box_info.get_visible ())
	        box_info.set_visible (false);
	}

    private void subprocess_scanimage (ScanImageType type, string? args=null, string? uri=null) throws GLib.Error {
        GLib.Subprocess subprocess = null;
        GLib.InputStream input_stream = null;
        GLib.DataInputStream dis = null;

        string username = GLib.Path.get_basename (PF.UserUtils.get_real_user_home ());
        if (type != ScanImageType.DEVICE_SCAN) {
            if (uri == null)
#if ELEMENTARY
                subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE, SCANNER_EXEC, args);
#else
                subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE, "sudo", SCANNER_EXEC, args, username);
#endif
            else {
#if ELEMENTARY
                subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE, SCANNER_EXEC, args, "-d", uri);
#else
                subprocess = new GLib.Subprocess (GLib.SubprocessFlags.STDOUT_PIPE,  "sudo", SCANNER_EXEC, args, username, uri);
#endif
            }
            input_stream = subprocess.get_stdout_pipe ();
            dis = new GLib.DataInputStream (input_stream);
        }

        switch (type) {
            case ScanImageType.FIND_SCANNERS:
                parse_scanners_found.@begin (dis, (obj, res) => {
                    var scanners = parse_scanners_found.@end (res);
                    search_scanner.set_visible (true);
                    display_scanners_found (scanners);
                    box_bottom_actions.set_visible (false);
                });
                display_progressbar (_(SEARCH_SCANNERS), StateUI.SEARCHING_SCANNER);
                break;
            case ScanImageType.OPTIONS_SCANNER:
                parse_scanner_options.@begin (dis, () => {
                    hide_progress ();
                    scanner_device.save_config ();
                    configure_options ();
                });
                break;
            case ScanImageType.DEVICE_SCAN:
                scanner_device.scan_with_script (SCANNER_EXEC,
                                                 args,
                                                 options_scanner.color_checked,
                                                 options_scanner.resolution_checked,
                                                 options_scanner.source_checked);
                scanner_device.scan_finished.connect (scan_finished_cb);
                break;
            default:
                break;
        }
    }

    private void scan_finished_cb (GLib.SList<string> files) {
        scanner_device.scan_finished.disconnect (scan_finished_cb);

        if (files != null && files.length () > 0) {
            uint nb_files = files.length ();
            if (nb_files > 1) {
                string assembling_pdf = _(LABEL_ASSEMBLING_IMAGES).printf (nb_files);
                display_progressbar (assembling_pdf, StateUI.ASSEMBLING_IMAGES);

                scanner_device.generate_pdf.begin ((obj, res) => {
                    var pdf = scanner_device.generate_pdf.end (res);
                    assembling_pdf_timeout (files.data, pdf);
                });
            } else {
                display_previewer (files.data);
                set_visible_boxes (true);
                hide_progress ();
            }
            state = StateUI.IDLE;
        } else {
            string scanner_not_found = _(SAVED_SCANNER_NOT_FOUND).printf (scanner_device.device.model);
            hide_progress ();
            state = StateUI.ERROR_FOUND;
            display_info_dialog (_(scanner_not_found));
        }
    }

    // We add timeout so that the user is informed
    // that we're generating pdf (the generation is too fast)
    private void assembling_pdf_timeout (string image_to_preview, string file_to_open) {
        GLib.Timeout.add_seconds (5, () => {
            display_previewer (image_to_preview, file_to_open);
            set_visible_boxes (true);
            hide_progress ();
            return GLib.Source.REMOVE;
        });
    }

    private async GLib.List<Device> parse_scanners_found (GLib.DataInputStream dis) {
	    GLib.List<Device> scanners = new GLib.List<Device> ();
		try {
	        string? line = null;
	        do {
	            line = yield dis.read_line_async ();
	            if (line == null)
                    break;

	            string[] device_info = line.split ("|");
	            if (device_info.length != 2) {
	                debug ("The line %s get from parse_scanners_found seems broken", line);
	                continue;
	            }

	            // Fix issue Photosmart_5510_series which display 2 devices instead of one
	            // with hplip 3.16.
                if (device_info[0].contains ("\""))
                    continue;

	            Device sd = new Device ();
	            sd.uri = device_info[0].strip ();
	            sd.model = device_info[1];

	            int index_pilote = device_info [0].index_of (":");
		        sd.pilote = device_info [0].substring (0, index_pilote);
                if (sd.uri.contains ("libusb") ||
                    sd.uri.contains (":bus") ||
                    sd.pilote == "imagescan")
                    sd.uri = sd.pilote;

				scanners.prepend (sd);
	        } while (line != null);
	    } catch (Error error) {
	        critical ("Error while starting scanimage: %s", error.message);
	    }
        return scanners;
    }

    private void display_scanners_found (GLib.List<Device> scanners) {
        uint nb_scanners = scanners.length ();

        if (nb_scanners == 0) {
            display_label_info (_(SCANNERS_NOT_FOUND), StateUI.ERROR_FOUND);
        } else if (nb_scanners == 1) {
            var scanner = scanners.data;
            find_details_scanner (scanner.uri, scanner.model);
        } else {
            if (scanners_iconview == null)
                create_iconview ();
             else
                scanners_model.clear ();

            var printer = printer_icon ();
		    scanners.@foreach ( (sd) => {
                Gtk.TreeIter iter;
                scanners_model.append (out iter);
                scanners_model.set (iter, ScannerInfo.ICON, printer,
                                    ScannerInfo.URI, sd.uri,
                                    ScannerInfo.MODEL, sd.model,
                                    ScannerInfo.PILOTE, _(PILOTE) + sd.pilote);
		    });
            scanners_iconview.model = scanners_model;
            display_label_info (_(LABEL_CHOOSE), StateUI.SCANNERS_FOUND);
        }
        hide_progress ();

        return;
    }

	private async void parse_scanner_options (GLib.DataInputStream dis) {
	    try {
            string? line = null;
            string mode = "--mode ";
	        string resolution = "--resolution ";
	        string source = "--source ";
	        do {
	            line = yield dis.read_line_async ();
	            if (line == null)
	                break;

	            line = line.strip ();

	            var index_default = line.index_of (" ["); // Default value.
	            line = line.substring (0, index_default);

                GLib.SList<string> options = new GLib.SList<string> ();
	            if (line.has_prefix (mode)) {
                    var line_splitted = line.replace(mode, "").split ("|");
		            foreach (unowned string str in line_splitted)
		                options.append (str);
		            scanner_device.set_modes_from_list (options);
                } else if (line.has_prefix (resolution)) {
                    var line_splitted = line.replace(resolution, "").split ("|");
                    if (line_splitted.length == 1) { // On epsonscan2:ET-2750 the resolution is A..C instead A|B|C
                        warning ("Splitted resolution == 1 : %s", line_splitted [0]);
                        if (line_splitted [0].contains ("..")) {
                            var interval = line_splitted [0].split ("..");
                            foreach (var i in interval) {
#if ELEMENTARY
                                options.append (i);
#else
                                if (int.parse (i) < 100)
                                    options.append ("100");
                                else if (int.parse (i) > 600)
                                    options.append ("600");
                                else
                                    warning ("Could not parse : %s", i);
#endif
                            }
                        }
                    } else {
                        foreach (unowned string str in line_splitted)
                            options.append (str);
                    }
		            scanner_device.set_resolutions_from_list (options);
                } else if (line.has_prefix (source)) {
                    var line_splitted = line.replace(source, "").split ("|");
		            foreach (unowned string str in line_splitted) {
		                if (str.contains ("ADF"))
    		                options.append (str);
    		            else {
    		                options.prepend (str);
    		            }
                    }
                    scanner_device.set_sources_from_list (options);
                }
	        } while (line != null);
	    } catch (Error error) {
	        critical ("Error while parsing scanner options : %s", error.message);
	    }
	}

    private void create_iconview () {
        scanners_model = new Gtk.ListStore (4,
										    typeof (Gdk.Pixbuf), // ICON
										    typeof (string),     // URI
										    typeof (string),     // Model
										    typeof (string));    // Pilote

		scanners_iconview = new Gtk.IconView () {
	        valign=Gtk.Align.CENTER,
		    halign=Gtk.Align.CENTER,
		    pixbuf_column=ScannerInfo.ICON,
		    activate_on_single_click=true,
		    columns=3
		};

		try {
            var provider = new Gtk.CssProvider ();
		    string data = ".view { background-color: transparent; }";
		    provider.load_from_data (data);
            scanners_iconview.get_style_context ().add_provider (provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
		} catch (Error e) {
		    warning ("Could not apply transparency to the iconview : %s", e.message);
		}

		var text_renderer = new Gtk.CellRendererText () {
		    alignment=Pango.Alignment.CENTER,
		    xalign=0.5f
		};

		scanners_iconview.pack_end (text_renderer, false);
		scanners_iconview.set_attributes (text_renderer, "text", ScannerInfo.PILOTE);
		scanners_iconview.item_activated.connect (configure_scanner);

		scanners_iconview.text_column = ScannerInfo.MODEL;
    }

    private void find_details_scanner (string uri, string model) {
        scanner_device.device.uri = uri;
		scanner_device.device.model = model;

		launch_subprocess (ScanImageType.OPTIONS_SCANNER, SCANNER_DETAILS, scanner_device.device.uri);

		string configure_model = _(CONFIGURATION_IN_PROGRESS).printf (scanner_device.device.model);
		display_progressbar (configure_model, StateUI.CONFIGURE_SCANNER);
    }

    private void configure_scanner (Gtk.TreePath path) {
        Gtk.TreeIter iter;
		if (scanners_model.get_iter (out iter, path)) {
		    string uri;
			string model;

			scanners_model.@get (iter, ScannerInfo.URI, out uri, ScannerInfo.MODEL, out model);
            find_details_scanner (uri, model);
		} else
            warning ("No item selected");
    }

	private Gdk.Pixbuf printer_icon () {
		var icon_theme = Gtk.IconTheme.get_default ();
		Gdk.Pixbuf? pix = null;

		try {
			pix = icon_theme.load_icon ("printer", 90, 0);
		} catch (Error e) {
			warning ("Printer icon not found");
		}
		return pix;
	}

    private void configure_options () {
        if (box_info != null) {
            box_info.destroy ();
            scanners_iconview = null;
            box_info = null;
        }

        is_running = false;

        if (options_scanner == null) {
            options_scanner = new BoxOptions (scanner_device);
            hbox.pack_start (options_scanner, false, false);
		    button_scan = new Gtk.Button.with_label (_(LABEL_SCAN)) {
		        valign=Gtk.Align.CENTER,
		        halign=Gtk.Align.START,
		        width_request=300,
		        margin_start=10
		    };

		    button_scan.enter_notify_event.connect (on_enter_notify_button);
		    button_scan.leave_notify_event.connect (on_leave_notify_button);


            button_scan.get_style_context ().add_class ("button-scanner");
            button_scan.button_press_event.connect (start_scan);
            box_bottom_actions.pack_start (button_scan, false, false);
        } else {
            // The modes and the resolutions (oustide elementary) are always the same
            // But the source may change.
            options_scanner.update_sources ();
#if ELEMENTARY
            options_scanner.add_resolutions_range ();
#endif
            button_scan.set_visible (true);
        }

        options_scanner.show_all ();
        box_bottom_actions.show_all();

        if (buttonbox_actions_right != null)
            buttonbox_actions_right.set_visible (false);

        state = StateUI.SCANNER_CONFIGURED;
    }

    private void display_previewer (string image_to_preview, string? file_to_open = null) {
        if (previewer_scanner == null) {
            previewer_scanner = new BoxOutputScanner ();
            hbox.pack_end (previewer_scanner, false, false);
        }

        if (buttonbox_actions_right == null) {
            buttonbox_actions_right = new Gtk.ButtonBox (Gtk.Orientation.VERTICAL) {
#if ELEMENTARY
                layout_style=Gtk.ButtonBoxStyle.SPREAD,
#else
                layout_style=Gtk.ButtonBoxStyle.EXPAND,
#endif
                halign=Gtk.Align.END,
                margin_end=20
            };

            var button_1 = new Gtk.Button.with_label (_("Send by email")) {
                width_request=375
            };
            button_1.get_style_context ().add_class ("button-scanner");
            button_1.button_press_event.connect (send_by_email_cb);

            // Change cursor hover button
		    button_1.enter_notify_event.connect (on_enter_notify_button);
		    button_1.leave_notify_event.connect (on_leave_notify_button);

            buttonbox_actions_right.add (button_1);

            var button_2 = new Gtk.Button.with_label (_("Save")){
                width_request=375
            };
            button_2.get_style_context ().add_class ("button-scanner");
            button_2.button_press_event.connect (save_scanned_files_cb);

            // Change cursor hover button
		    button_2.enter_notify_event.connect (on_enter_notify_button);
		    button_2.leave_notify_event.connect (on_leave_notify_button);

            buttonbox_actions_right.add (button_2);

            box_bottom_actions.pack_end (buttonbox_actions_right);
        }

        previewer_scanner.update_previewer (image_to_preview, file_to_open);

        previewer_scanner.show_all ();

    }

    private bool start_scan () {
        is_running = true;
        display_progressbar (_(SCAN_IN_PROGRESS), StateUI.SCANNING);

        set_visible_boxes (false);
        search_scanner.set_visible (false);

        launch_subprocess (ScanImageType.DEVICE_SCAN, SCANNER_SCAN);
        return false;
    }

    private bool send_by_email_cb () {
        scanner_device.send_by_email (previewer_scanner.get_entry ());
        return false;
    }

    private void display_info_dialog (string msg) {
		var info_dialog = new Gtk.MessageDialog (window as Gtk.Window,
		                                         Gtk.DialogFlags.DESTROY_WITH_PARENT | Gtk.DialogFlags.MODAL,
										         Gtk.MessageType.INFO, Gtk.ButtonsType.NONE, null) {
										             secondary_text=msg
										         };
		info_dialog.set_decorated (false);

        var button_ok = info_dialog.add_button ("Ok", Gtk.ResponseType.ACCEPT);
        button_ok.halign = Gtk.Align.END;
        button_ok.width_request = 45;
#if !(ELEMENTARY)
        button_ok.get_style_context ().add_class ("circular");
#endif

	    if (info_dialog.run () == Gtk.ResponseType.ACCEPT) {
	        if (state == StateUI.ERROR_FOUND) {
	            is_running=false;
	            search_scanner.show_all ();
	            options_scanner.set_visible (true);
	            box_bottom_actions.show_all ();
                if (buttonbox_actions_right != null)
                    buttonbox_actions_right.set_visible (false);
            }
	    }
        info_dialog.destroy ();
    }

    private bool save_scanned_files_cb () {
        string input_entry = previewer_scanner.get_entry ();
        if (input_entry.strip () == "") {
            warning ("Trying to save with an empty name.");
            return true;
        }

        string saved_file = scanner_device.save_scanned_files (input_entry);

        if (saved_file != null) {
            message ("SAVED File : %s", saved_file);
            if (saved_file.has_suffix (".jpeg"))
                saved_file = saved_file.replace (".jpeg", "");
            else if (saved_file.has_suffix (".pdf"))
                saved_file = saved_file.replace (".pdf", "");
            display_info_dialog (_(LABEL_FILE_SAVED).printf (saved_file));
        }
        return false;
    }

	private bool on_enter_notify_button (Gtk.Widget widget, Gdk.EventCrossing event) {
		Gdk.Cursor pointer = new Gdk.Cursor.from_name (Gdk.Display.get_default(), "pointer");
		((Gdk.Window)widget.get_parent_window()).set_cursor (pointer);
		return true;
	}

	private bool on_leave_notify_button (Gtk.Widget widget, Gdk.EventCrossing event) {
		((Gdk.Window)widget.get_parent_window()).set_cursor (null);
		return true;
	}
}

public Files.Plugins.Base module_init () {
    return new Files.Plugins.Scanners ();
}
