/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public class BoxOutputScanner: Gtk.Box {
    private Gtk.EventBox image_event;
    private Gtk.Image image_previewer;

    private Gtk.Label document_label;
	private Gtk.Entry document_name;

	private GLib.File file_previewer;
	private GLib.File file_to_open;

	private const string DOCUMENT_LABEL = N_("Document name :");

    private string current_date;

	construct {
		this.orientation = Gtk.Orientation.VERTICAL;
        this.halign = Gtk.Align.END;
        this.margin_end = 20;
        this.vexpand = true;
        this.spacing = 20;
	}

    public BoxOutputScanner () {
        image_event = new Gtk.EventBox ();
        image_previewer = new Gtk.Image ();

		image_event.add (image_previewer);
		image_event.enter_notify_event.connect (on_enter_notify_image);
		image_event.leave_notify_event.connect (on_leave_notify_image);
		image_event.button_press_event.connect (open_file);
		image_previewer.get_style_context ().add_class (Gtk.STYLE_CLASS_FRAME);


		document_label = new Gtk.Label (_(DOCUMENT_LABEL)) {
			xalign=0
		};
		document_label.get_style_context ().add_class (Granite.STYLE_CLASS_H1_LABEL);

		document_name = new Gtk.Entry () {
			width_request=375
		};
		document_name.get_style_context().add_class (Gtk.STYLE_CLASS_FRAME);
		document_name.insert_text.connect ((new_character, new_text_length, ref position) => {
			if (new_character.get_char ().isalnum() == false) {
				if ((position == 0 && new_character.get_char ().isspace()) ||
					new_character != "-" &&  new_character != "_" && !new_character.get_char ().isspace())
					GLib.Signal.stop_emission_by_name (document_name, "insert-text");
			}
		});

		GLib.DateTime time  = new GLib.DateTime.now ();
		current_date = time.format ("Scan %d %B %Y");
		document_name.set_text (current_date);

		var scroll = new Gtk.ScrolledWindow (null, null) {
			hscrollbar_policy=Gtk.PolicyType.NEVER,
			vscrollbar_policy=Gtk.PolicyType.AUTOMATIC
		};
		scroll.add (image_event);

		this.pack_start (scroll, true, true);
		this.pack_start (document_label, false, false);
		this.pack_start (document_name, false, false);
	}

	public void update_previewer (string filename, string? file) {
		try {
			Gdk.Pixbuf pixbuf_previewer = new Gdk.Pixbuf.from_file_at_scale (filename, -1, 520, true);

			file_previewer = GLib.File.new_for_path (filename);
			image_previewer.set_from_pixbuf (pixbuf_previewer);
		} catch (Error e) {
			critical (e.message);
		}

		if (file != null) {
			file_to_open = GLib.File.new_for_path (file);
		} else if (file_to_open != null)
			file_to_open = null;
	}

	private bool open_file () {
		GLib.FileInfo? info = null;
		try {
			if (file_to_open != null) {
				info = file_to_open.query_info (GLib.FileAttribute.STANDARD_CONTENT_TYPE, GLib.FileQueryInfoFlags.NONE);
			} else
				info = file_previewer.query_info (GLib.FileAttribute.STANDARD_CONTENT_TYPE, GLib.FileQueryInfoFlags.NONE);
		} catch (Error e) {
			warning ("Coult not retrieve info from the scanned image : %s", e.message);
		}

		unowned string? ftype = info.get_attribute_string (GLib.FileAttribute.STANDARD_CONTENT_TYPE);
		AppInfo app = GLib.AppInfo.get_default_for_type (ftype, true);//file.get_default_handler ();
		GLib.List <GLib.File> list_files = new GLib.List <GLib.File>();

		if (file_to_open != null)
		list_files.append (file_to_open);
		else
			list_files.append (file_previewer);

		try {
			app.launch (list_files, null);
		} catch (Error e) {
			warning ("Cannot open image scanned. Error : %s", e.message);
		}
		return true;
	}

	private bool on_enter_notify_image () {
		Gdk.Cursor pointer = new Gdk.Cursor.from_name (Gdk.Display.get_default(), "pointer");
		((Gdk.Window)image_event.get_parent_window()).set_cursor (pointer);
		return true;
	}

	private bool on_leave_notify_image () {
		((Gdk.Window)image_event.get_parent_window()).set_cursor (null);
		return true;
	}

	public string get_entry () {
	    return document_name.text;
	}
}
