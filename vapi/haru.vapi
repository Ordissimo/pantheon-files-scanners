[CCode(cheader_filename="hpdf.h", cprefix="HPDF_")]
namespace HPDF {
    [CCode(cname="HPDF_STATUS")]
    public struct Status : ulong {
    }

    [CCode(cname="HPDF_REAL")]
    public struct Real : float {
    }

    [CCode(cname="HPDF_UINT")]
    public struct Uint : uint {
    }

    public const Uint COMP_NONE;
    public const Uint COMP_TEXT;
    public const Uint COMP_IMAGE;
    public const Uint COMP_METADATA;
    public const Uint COMP_ALL;

    [CCode(cname="HPDF_Error_Handler", instance_pos = -1)]
    public delegate void ErrorHandler (Status error_no, Status detail_no);

    [Compact]
    [CCode(free_function="HPDF_Free", cname="HPDF_Doc")]
    public class Doc {
        [CCode(cname="HPDF_New", instance_pos = -1)]
        public Doc (ErrorHandler error_handler);

        [CCode(cname="HPDF_SetCompressionMode")]
        public Status set_compression_mode (Uint value);

        [CCode(cname="HPDF_AddPage")]
        public unowned Page add_page();

        [CCode(cname="HPDF_GetFont")]
        public unowned Font get_font(string name, string? encoding = null);

        [CCode(cname="HPDF_LoadJpegImageFromFile")]
        public unowned Image load_jpeg_image_from_file (string filename);

        [CCode(cname="HPDF_SaveToFile")]
        public Status save_to_file (string file);
    }

    [Compact]
    [CCode(cname="HPDF_Page")]
    public class Page {
        [CCode(cname="HPDF_Page_SetFontAndSize")]
        public Status set_font_and_size (Font font, float size);

        [CCode(cname="HPDF_Page_BeginText")]
        public Status begin_text ();

        [CCode(cname="HPDF_Page_EndText")]
        public Status end_text ();

        [CCode(cname="HPDF_Page_TextOut")]
        public Status text_out (Real x, Real y, string chars);

        [CCode(cname="HPDF_Page_SetCharSpace")]
        public Status set_char_space (Real value);

        [CCode(cname="HPDF_Page_SetWordSpace")]
        public Status set_word_space (Real value);

        [CCode(cname="HPDF_Page_SetWidth")]
        public Status set_width (Real value);

        [CCode(cname="HPDF_Page_SetHeight")]
        public Status set_height (Real value);

        [CCode(cname="HPDF_Page_DrawImage")]
        public Status draw_image (Image image, Real x, Real y, Real width, Real height);
    }

    [Compact]
    [CCode(cname="HPDF_Image")]
    public class Image {
        [CCode(cname="HPDF_Image_GetWidth")]
        public Uint get_width ();

        [CCode(cname="HPDF_Image_GetHeight")]
        public Uint get_height ();
    }

    [Compact]
    [CCode(cname="HPDF_Font")]
    public class Font {
    }
}
