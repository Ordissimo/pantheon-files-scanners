Pantheon-files-scanners <br>
<br>
Description : <br>
This plugin add device which on click will display a interface simple interface for searching scanners and scanning. <br>
<br>

```
Requirements :
    For the execution:
         All requirements from Pantheon-files.
	 sane-utils.

    For the build:
         meson
         posix
         glib-2.0
         gee-0.8
	 gtk+-3.0
	 granite
	 pantheon-files-core
	 libhpdf
```

Install :<br>
To install this plugin :<br>
<br>
```
    meson build
    cd build
    sudo ninja install
```


License :<br>
This program is released under the terms of the GNU General Public License. <br>
Please see the file COPYING for details.<br>
<br>
